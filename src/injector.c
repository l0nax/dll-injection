#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <tlhelp32.h>

const char *dllPath = "C:\\Users\\IEUser\\Desktop\\Mydll.dll";
const char procName[] = "notepad.exe";
const char* buffer = "C:\\Users\\IEUser\\Desktop\\Mydll.dll";

int FindProcessId(const char *procname);
void SetDebugPrivilege();

int main(int argc, char* argv[]) { 
	SetDebugPrivilege();

	int procID = FindProcessId(procName);

	printf("PID: %d\n", procID);

	HANDLE process = OpenProcess(PROCESS_ALL_ACCESS, FALSE, procID);
	if(process == NULL)
		printf("Error: the specified process couldn't be found.\n");

 

	LPVOID addr = (LPVOID)GetProcAddress(GetModuleHandle("kernel32.dll"), "LoadLibraryA");
	if(addr == NULL)
		printf("Error: the LoadLibraryA function was not found inside kernel32.dll library.\n");
	
 
	
	LPVOID arg = (LPVOID)VirtualAllocEx(process, NULL, strlen(buffer), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	if(arg == NULL)
		printf("Error: the memory could not be allocated inside the chosen process.\n");
		
		 
	
	int n = WriteProcessMemory(process, arg, buffer, strlen(buffer), NULL);
	if(n == 0)
		printf("Error: there was no bytes written to the process's address space.\n");
	
 
	
	HANDLE threadID = CreateRemoteThread(process, NULL, 0, (LPTHREAD_START_ROUTINE)addr, arg, NULL, NULL);
	if(threadID == NULL)
		printf("Error: the remote thread could not be created.\n");
	else 
		printf("Success: the remote thread was successfully created.\n");
	 
	CloseHandle(process);
	getchar();
	 
	return 0;
}

int FindProcessId(const char *procname) {
	HANDLE hProcSnap;
	PROCESSENTRY32 pe32;
	DWORD result = NULL;

	// take snapshot of all processes
	hProcSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if(INVALID_HANDLE_VALUE == hProcSnap)
		return FALSE;

	pe32.dwSize = sizeof(PROCESSENTRY32);

	// get informations from the first process
	if(!Process32First(hProcSnap, &pe32)) {
		CloseHandle(hProcSnap);
		return NULL;
	}

	do {
		if(0 == strcmp(procname, pe32.szExeFile)) {
			result = pe32.th32ProcessID;
			break;
		}
	} while(Process32Next(hProcSnap, &pe32));

	CloseHandle(hProcSnap);
	return result;
}

void SetDebugPrivilege() {
        HANDLE hProcess = GetCurrentProcess(), hToken;
        TOKEN_PRIVILEGES priv;
        LUID luid;

        OpenProcessToken(hProcess, TOKEN_ADJUST_PRIVILEGES, &hToken);

        LookupPrivilegeValue(0, "seDebugPrivilege", &luid);

        CloseHandle(hToken);
}
