#!/bin/bash
echo ""
echo ""
echo "*** Script zum Kopieren der DLL's & exe Datei zu /tmp/_testing/ ***"

rm -rf /tmp/_testing/
mkdir /tmp/_testing/ > /dev/null

echo "!! Clearing /tmp/_testing/ Directory !!"
rm -rf /tmp/_testing/*

find . -name *.dll -exec cp {} /tmp/_testing \;
find . -name *.exe -exec cp {} /tmp/_testing \;

